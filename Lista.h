#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct Variable{
	char *nombre;
	int tipo;
	char *valor;
  	struct Variable *sig;
}Nodo;

typedef struct{
	Nodo *var;
} Lista;

int insertar( Lista **, char *, int, char *);
void actualizar(Lista ** ini, char * nomb, char * val);
Nodo * crearVariable(char *, int, char *);
void mostrarLista(Lista *ini);
Nodo * buscar(Lista *, char * );
int validarTipo(int tipo,int tipoOP);
int validarTipoVar(char *nomb,int tipoOP);
char* obtenerValor(char *nomb);
int compararIF(float op1,float op2, int oper);
void imprimirBool(int c);

