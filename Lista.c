#include "Lista.h"

Nodo* crearVariable(char *nomb, int tipo, char *val){
  Nodo *var = (Nodo*)malloc(sizeof(Nodo));
  var->nombre  = (char*) malloc(sizeof(nomb)); 
  var->valor  = (char*) malloc(sizeof(val));  
  strcpy( var->nombre, nomb );
  strcpy( var->valor, val );
  var-> tipo = tipo;
  var -> sig = NULL;
  return var;
}

int insertar( Lista **head, char * nomb, int tipo, char * val){ 
  Nodo *aux = crearVariable(nomb,tipo,val);
  if ((*head)->var == NULL){
    (*head)->var = aux;
    return 0;
  }
  Nodo *iter = (*head)->var;  
  while(iter->sig != NULL)
    iter = iter->sig;
  iter -> sig =  aux;
  return 1;
    
} 

void mostrarLista(Lista *ini){
    Nodo *iter;
    iter=ini->var;
    while(iter!=NULL){
      printf("|Variable: <%s> = %s [%d]\n",iter->nombre,iter->valor, iter->tipo);
        iter=iter->sig;
    }
    printf("\n");
}

Nodo * buscar(Lista * ini, char * s) {
  if (!ini->var) 
    return 0;  
  Nodo ** x =  &(ini->var);
  while ((*x)) {
    if (strcmp((*x)->nombre, s) == 0){
      return *x;
    }
    x = &(*x)->sig;
  }
  return 0;
}

void actualizar(Lista ** ini, char * nomb, char * val){
  if (!(*ini)->var)   
    return; 
  Nodo * aux = buscar((*ini), nomb);
  Nodo  ** x =  &aux; 
  (*x)->valor = (char*) realloc((*x)->valor,sizeof(val));
  strcpy((*x)->valor, val);
  return;
}