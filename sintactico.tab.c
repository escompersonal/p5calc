/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "sintactico.y" /* yacc.c:339  */

#include "Lista.h"
#include <math.h>


Nodo *aux;
#line 9 "sintactico.y" /* yacc.c:339  */

   	Lista *head;
   	char *cade;

	char * concat(char * a, char * b) {
	cade = (char*) malloc(sizeof(a)+sizeof(b));
	strcpy(cade, ""); 
	strcat(cade,  a); 
	strcat(cade,  b); 
	return strdup(cade);    
}

  	


#line 89 "sintactico.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "sintactico.tab.h".  */
#ifndef YY_YY_SINTACTICO_TAB_H_INCLUDED
# define YY_YY_SINTACTICO_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    ENTERO = 258,
    CADENA = 259,
    INC_OP = 260,
    FPOW = 261,
    IF = 262,
    REAL = 263,
    TIPO_DATO = 264,
    INTID = 265,
    UNDECLRID = 266,
    FLOATID = 267,
    STRINGID = 268,
    MENOR = 269,
    MAYOR = 270,
    IGUAL = 271,
    MAS = 272,
    MENOS = 273,
    POR = 274,
    ENTRE = 275,
    POT = 276,
    MOD = 277,
    PA = 278,
    PC = 279,
    COMA = 280,
    PTCOMA = 281,
    DIV = 282,
    POW = 283
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE YYSTYPE;
union YYSTYPE
{
#line 26 "sintactico.y" /* yacc.c:355  */

	int entero;
	float real;
	char* cadena;

#line 164 "sintactico.tab.c" /* yacc.c:355  */
};
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_SINTACTICO_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 179 "sintactico.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  2
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   463

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  30
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  14
/* YYNRULES -- Number of rules.  */
#define YYNRULES  103
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  198

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   283

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      29,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,    83,    83,    84,    87,    88,    89,    90,    91,    92,
      93,    94,    95,    96,    99,   100,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   142,   143,   144,   147,
     148,   149,   153,   154,   155,   156,   160,   161,   162,   163,
     164,   166,   167,   168,   169,   171,   172,   173,   175,   176,
     177,   179,   180,   181,   182,   184,   185,   186,   187,   188,
     192,   193,   194,   197,   198,   199,   200,   201,   209,   210,
     217,   224,   229,   230,   231,   232,   238,   246,   254,   260,
     261,   262,   263,   264
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "ENTERO", "CADENA", "INC_OP", "FPOW",
  "IF", "REAL", "TIPO_DATO", "INTID", "UNDECLRID", "FLOATID", "STRINGID",
  "MENOR", "MAYOR", "IGUAL", "MAS", "MENOS", "POR", "ENTRE", "POT", "MOD",
  "PA", "PC", "COMA", "PTCOMA", "DIV", "POW", "'\\n'", "$accept", "input",
  "line", "operacion", "exp_ent", "exp_real", "exp_cad", "comp", "func_if",
  "inc_op", "var_declr", "opers", "declr", "asign", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,    10
};
# endif

#define YYPACT_NINF -19

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-19)))

#define YYTABLE_NINF -83

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     -19,     1,   -19,   -19,   -19,    -8,    10,   -19,    43,   -10,
       6,     2,     5,    26,   -19,   -19,    34,   397,   403,   189,
      64,   320,    20,    65,    66,    17,   237,   -19,    23,   -19,
     -19,    56,    47,   110,    68,    73,   -19,   -19,    74,   165,
     176,   176,   237,   237,   176,   259,   275,   176,   176,   237,
     237,   176,   286,   307,   -19,   -19,   -19,   -19,   -19,    75,
     121,    78,   237,    79,   318,   110,    80,    86,   -19,    91,
     348,   -19,   333,   342,   318,   110,   -19,   -19,   -19,   397,
     403,   409,   415,   -19,   -19,   -19,   -19,    46,   207,   281,
     281,   313,   437,   -18,    37,   421,   427,   421,   427,   -18,
      37,   -19,   397,   403,   409,   415,   -19,    46,   207,   281,
     281,   421,   437,   -18,    37,   421,   427,   421,   427,   -18,
      37,   -19,   409,   415,   -19,   313,   437,   -19,   -19,   -19,
     421,   427,   -19,   313,   437,   -19,   421,   427,   100,   397,
     403,   409,   415,   -19,   -19,    17,    17,    17,    17,    17,
      17,    17,   -19,   -19,   103,   237,   237,   237,   237,   237,
     237,   237,   237,   237,   237,   237,   237,   237,   237,   421,
     427,   100,   397,   403,   409,   415,    13,   441,   441,    50,
     433,   433,    50,   357,   -19,   313,   437,   365,   373,   313,
     437,   381,   389,   -19,   -19,   -19,   -19,   -19
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       2,     0,     1,    16,    46,     0,     0,    26,     0,    25,
      79,    34,    48,     0,     4,     3,     0,    14,    15,     0,
       0,     0,     0,     0,     0,     0,     0,    80,    88,    81,
      82,     0,     0,     0,     0,     0,    17,    27,     0,     0,
      83,    84,    85,    86,    87,     0,     0,    83,    84,    85,
      86,    87,     0,     0,    83,    84,    85,    86,    87,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    25,     0,
       0,    34,     0,     0,     0,     0,     9,    79,    48,   101,
     102,   100,   103,    10,    11,     5,    69,    18,    36,    76,
      62,    19,    37,    20,    40,    21,    38,    22,    42,    23,
      44,    66,     0,     0,    72,    57,    68,    35,    28,    77,
      64,     0,    29,    41,    31,    39,    30,    43,    32,    45,
      33,    65,    74,    59,    70,    75,    78,    47,     6,    67,
      71,    73,    13,    61,    63,    12,    56,    58,    60,    96,
      97,    98,    99,     7,     8,     0,     0,     0,     0,     0,
       0,     0,    49,    50,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    89,
      90,    91,    93,    94,    95,    92,     0,    18,    19,    20,
      21,    22,    23,     0,    51,    18,    36,     0,     0,    35,
      28,     0,     0,    24,    52,    55,    54,    53
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
     -19,   -19,   -19,   -19,    -1,    36,   186,    22,   -19,    60,
     118,   134,   -19,   -19
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,    15,    16,    91,    92,   127,   161,    20,    21,
      22,    60,    23,    24
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      17,     2,   158,   159,     3,     4,   -80,     5,     6,     7,
       8,     9,    10,    11,    12,    25,    32,     4,   -81,    13,
       3,   -82,    33,     5,    70,    72,    78,    68,    34,    36,
      14,    35,    79,    26,    37,    69,    65,    18,    87,    74,
      93,    95,    97,    99,   102,   107,   111,   113,   115,   117,
     119,   102,   125,    27,    28,    29,    30,   165,   166,   130,
      38,   133,    73,   136,   139,    41,    42,    43,    44,    80,
     148,   149,    75,   169,   172,    88,    76,    94,    96,    98,
     100,   103,   108,   112,   114,   116,   118,   120,   103,   126,
      61,    66,    67,    82,    36,   168,   131,    83,   134,    90,
     137,   140,    84,    85,   128,   105,   110,   132,   135,   143,
     170,   173,   123,     3,     4,   144,     5,   176,     7,   184,
      68,    77,    71,    78,     3,   142,    31,     5,    13,     7,
       0,    68,   129,    71,     0,   175,     0,     0,     0,    13,
       0,     0,     0,     0,   177,   178,   179,   180,   181,   182,
     183,    45,    52,     0,   185,    64,    93,    95,    97,    99,
     187,   189,   111,   113,   115,   117,   119,   191,     3,     4,
       0,     5,     0,     7,     0,    68,    86,    71,    78,     3,
       0,     0,     5,    13,     7,     0,    68,    19,    71,     0,
       0,   186,     0,    94,    96,    98,   100,   188,   190,   112,
     114,   116,   118,   120,   192,     0,    53,    54,    55,    56,
      57,    58,     0,    45,    52,    59,    64,     0,     0,    81,
       0,    45,    52,     0,    64,    89,    48,    49,    50,    51,
       0,   104,   109,     0,     0,     0,    45,    52,   122,    64,
       3,    45,    52,     5,    64,     7,     0,    68,     0,    71,
     138,   141,     0,     0,     0,    13,     0,    64,     0,     0,
     171,   174,     3,     4,     0,     5,     0,     7,     0,    68,
     101,    71,    78,    45,    52,     0,    64,    13,     3,     4,
       0,     5,     0,     7,     0,    68,   106,    71,    78,     3,
       4,     0,     5,    13,     7,     0,    68,   121,    71,    78,
      55,    56,    57,    58,    13,     0,    45,    52,     0,    64,
       3,     4,     0,     5,     0,     7,     0,    68,   124,    71,
      78,     3,     4,     0,     5,    13,     7,     0,    68,     0,
      71,    78,   157,   158,   159,   160,    13,    62,    54,    55,
      56,    57,    58,     0,     0,     0,    63,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   152,   153,   154,   162,
     163,   164,   165,   166,   167,   145,   146,   147,   148,   149,
     150,     0,     0,   151,   145,   146,   147,   148,   149,   150,
       0,   193,   155,   156,   157,   158,   159,   160,     0,   194,
     162,   163,   164,   165,   166,   167,     0,   195,   155,   156,
     157,   158,   159,   160,     0,   196,   162,   163,   164,   165,
     166,   167,     0,   197,    39,    40,    41,    42,    43,    44,
      46,    47,    48,    49,    50,    51,    53,    54,    55,    56,
      57,    58,    62,    54,    55,    56,    57,    58,   155,   156,
     157,   158,   159,   160,   162,   163,   164,   165,   166,   167,
     145,   146,   147,   148,   149,   150,   164,   165,   166,   167,
     147,   148,   149,   150
};

static const yytype_int16 yycheck[] =
{
       1,     0,    20,    21,     3,     4,    16,     6,     7,     8,
       9,    10,    11,    12,    13,    23,    26,     4,    16,    18,
       3,    16,    16,     6,    25,    26,    13,    10,    26,     3,
      29,    26,    33,    23,     8,    18,    16,     1,    39,    16,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    10,    11,    12,    13,    20,    21,    60,
      26,    62,    26,    64,    65,    19,    20,    21,    22,    33,
      20,    21,    16,    74,    75,    39,    29,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      26,    26,    26,    33,     3,    73,    60,    29,    62,    39,
      64,    65,    29,    29,    29,    45,    46,    29,    29,    29,
      74,    75,    52,     3,     4,    29,     6,    17,     8,    16,
      10,    11,    12,    13,     3,    65,     8,     6,    18,     8,
      -1,    10,    11,    12,    -1,    75,    -1,    -1,    -1,    18,
      -1,    -1,    -1,    -1,   145,   146,   147,   148,   149,   150,
     151,    17,    18,    -1,   155,    21,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,   168,     3,     4,
      -1,     6,    -1,     8,    -1,    10,    11,    12,    13,     3,
      -1,    -1,     6,    18,     8,    -1,    10,     1,    12,    -1,
      -1,   155,    -1,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,    -1,    17,    18,    19,    20,
      21,    22,    -1,    79,    80,    26,    82,    -1,    -1,    33,
      -1,    87,    88,    -1,    90,    39,    19,    20,    21,    22,
      -1,    45,    46,    -1,    -1,    -1,   102,   103,    52,   105,
       3,   107,   108,     6,   110,     8,    -1,    10,    -1,    12,
      64,    65,    -1,    -1,    -1,    18,    -1,   123,    -1,    -1,
      74,    75,     3,     4,    -1,     6,    -1,     8,    -1,    10,
      11,    12,    13,   139,   140,    -1,   142,    18,     3,     4,
      -1,     6,    -1,     8,    -1,    10,    11,    12,    13,     3,
       4,    -1,     6,    18,     8,    -1,    10,    11,    12,    13,
      19,    20,    21,    22,    18,    -1,   172,   173,    -1,   175,
       3,     4,    -1,     6,    -1,     8,    -1,    10,    11,    12,
      13,     3,     4,    -1,     6,    18,     8,    -1,    10,    -1,
      12,    13,    19,    20,    21,    22,    18,    17,    18,    19,
      20,    21,    22,    -1,    -1,    -1,    26,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    17,    18,    19,    20,    21,
      22,    -1,    -1,    25,    17,    18,    19,    20,    21,    22,
      -1,    24,    17,    18,    19,    20,    21,    22,    -1,    24,
      17,    18,    19,    20,    21,    22,    -1,    24,    17,    18,
      19,    20,    21,    22,    -1,    24,    17,    18,    19,    20,
      21,    22,    -1,    24,    17,    18,    19,    20,    21,    22,
      17,    18,    19,    20,    21,    22,    17,    18,    19,    20,
      21,    22,    17,    18,    19,    20,    21,    22,    17,    18,
      19,    20,    21,    22,    17,    18,    19,    20,    21,    22,
      17,    18,    19,    20,    21,    22,    19,    20,    21,    22,
      19,    20,    21,    22
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    31,     0,     3,     4,     6,     7,     8,     9,    10,
      11,    12,    13,    18,    29,    32,    33,    34,    35,    36,
      38,    39,    40,    42,    43,    23,    23,    10,    11,    12,
      13,    40,    26,    16,    26,    26,     3,     8,    26,    17,
      18,    19,    20,    21,    22,    41,    17,    18,    19,    20,
      21,    22,    41,    17,    18,    19,    20,    21,    22,    26,
      41,    26,    17,    26,    41,    16,    26,    26,    10,    18,
      34,    12,    34,    35,    16,    16,    29,    11,    13,    34,
      35,    36,    39,    29,    29,    29,    11,    34,    35,    36,
      39,    34,    35,    34,    35,    34,    35,    34,    35,    34,
      35,    11,    34,    35,    36,    39,    11,    34,    35,    36,
      39,    34,    35,    34,    35,    34,    35,    34,    35,    34,
      35,    11,    36,    39,    11,    34,    35,    36,    29,    11,
      34,    35,    29,    34,    35,    29,    34,    35,    36,    34,
      35,    36,    39,    29,    29,    17,    18,    19,    20,    21,
      22,    25,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    37,    17,    18,    19,    20,    21,    22,    37,    34,
      35,    36,    34,    35,    36,    39,    17,    34,    34,    34,
      34,    34,    34,    34,    16,    34,    35,    34,    35,    34,
      35,    34,    35,    24,    24,    24,    24,    24
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    30,    31,    31,    32,    32,    32,    32,    32,    32,
      32,    32,    32,    32,    33,    33,    34,    34,    34,    34,
      34,    34,    34,    34,    34,    34,    35,    35,    35,    35,
      35,    35,    35,    35,    35,    35,    35,    35,    35,    35,
      35,    35,    35,    35,    35,    35,    36,    36,    36,    37,
      37,    37,    38,    38,    38,    38,    39,    39,    39,    39,
      39,    39,    39,    39,    39,    39,    39,    39,    39,    39,
      39,    39,    39,    39,    39,    39,    39,    39,    39,    39,
      40,    40,    40,    41,    41,    41,    41,    41,    42,    42,
      42,    42,    42,    42,    42,    42,    43,    43,    43,    43,
      43,    43,    43,    43
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     0,     2,     1,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     1,     1,     1,     2,     3,     3,
       3,     3,     3,     3,     6,     1,     1,     2,     3,     3,
       3,     3,     3,     3,     1,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     1,     3,     1,     1,
       1,     2,     6,     6,     6,     6,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     2,     4,
       4,     4,     4,     4,     4,     4,     3,     3,     3,     3,
       3,     3,     3,     3
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 9:
#line 92 "sintactico.y" /* yacc.c:1646  */
    { printf("Valor: %d\n",atoi(obtenerValor((yyvsp[-2].cadena))));}
#line 1434 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 10:
#line 93 "sintactico.y" /* yacc.c:1646  */
    { printf("Valor: %f\n",atof(obtenerValor((yyvsp[-2].cadena))));}
#line 1440 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 11:
#line 94 "sintactico.y" /* yacc.c:1646  */
    { printf("Valor: %s\n",obtenerValor((yyvsp[-2].cadena)));}
#line 1446 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 13:
#line 96 "sintactico.y" /* yacc.c:1646  */
    {imprimirBool((yyvsp[-2].entero));}
#line 1452 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 14:
#line 99 "sintactico.y" /* yacc.c:1646  */
    {printf("Resultado: %d\n",(yyvsp[0].entero));}
#line 1458 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 15:
#line 100 "sintactico.y" /* yacc.c:1646  */
    {printf("Resultado: %f\n",(yyvsp[0].real));}
#line 1464 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 106 "sintactico.y" /* yacc.c:1646  */
    { (yyval.entero) = (yyvsp[0].entero); }
#line 1470 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 17:
#line 107 "sintactico.y" /* yacc.c:1646  */
    { (yyval.entero) = ((yyvsp[0].entero))*(-1);}
#line 1476 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 108 "sintactico.y" /* yacc.c:1646  */
    { (yyval.entero) = (yyvsp[-2].entero) + (yyvsp[0].entero);}
#line 1482 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 19:
#line 109 "sintactico.y" /* yacc.c:1646  */
    { (yyval.entero) = (yyvsp[-2].entero) - (yyvsp[0].entero);}
#line 1488 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 20:
#line 110 "sintactico.y" /* yacc.c:1646  */
    { (yyval.entero) = (yyvsp[-2].entero) * (yyvsp[0].entero);}
#line 1494 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 21:
#line 111 "sintactico.y" /* yacc.c:1646  */
    { (yyval.entero) = (yyvsp[-2].entero) / (yyvsp[0].entero);}
#line 1500 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 112 "sintactico.y" /* yacc.c:1646  */
    { (yyval.entero) = pow((yyvsp[-2].entero),(yyvsp[0].entero));}
#line 1506 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 23:
#line 113 "sintactico.y" /* yacc.c:1646  */
    { (yyval.entero) = fmod((yyvsp[-2].entero),(yyvsp[0].entero));}
#line 1512 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 24:
#line 114 "sintactico.y" /* yacc.c:1646  */
    { (yyval.entero) = pow((yyvsp[-3].entero),(yyvsp[-1].entero));}
#line 1518 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 25:
#line 115 "sintactico.y" /* yacc.c:1646  */
    { (yyval.entero) = atoi(obtenerValor((yyvsp[0].cadena)));}
#line 1524 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 26:
#line 119 "sintactico.y" /* yacc.c:1646  */
    {(yyval.real) = (yyvsp[0].real);}
#line 1530 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 27:
#line 120 "sintactico.y" /* yacc.c:1646  */
    { (yyval.real) = ((yyvsp[0].real))*(-1);}
#line 1536 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 28:
#line 121 "sintactico.y" /* yacc.c:1646  */
    { (yyval.real) = (yyvsp[-2].real) + (yyvsp[0].real);}
#line 1542 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 29:
#line 122 "sintactico.y" /* yacc.c:1646  */
    { (yyval.real) = (yyvsp[-2].real) - (yyvsp[0].real);}
#line 1548 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 30:
#line 123 "sintactico.y" /* yacc.c:1646  */
    { (yyval.real) = (yyvsp[-2].real) / (yyvsp[0].real);	}
#line 1554 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 31:
#line 124 "sintactico.y" /* yacc.c:1646  */
    { (yyval.real) = (yyvsp[-2].real) * (yyvsp[0].real);}
#line 1560 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 32:
#line 125 "sintactico.y" /* yacc.c:1646  */
    { (yyval.real) = pow((yyvsp[-2].real),(yyvsp[0].real));}
#line 1566 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 126 "sintactico.y" /* yacc.c:1646  */
    { (yyval.real) = fmod((yyvsp[-2].real),(yyvsp[0].real));}
#line 1572 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 34:
#line 127 "sintactico.y" /* yacc.c:1646  */
    { (yyval.real) = atof(obtenerValor((yyvsp[0].cadena)));}
#line 1578 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 35:
#line 129 "sintactico.y" /* yacc.c:1646  */
    { (yyval.real) = (yyvsp[-2].real) + (yyvsp[0].entero);}
#line 1584 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 36:
#line 130 "sintactico.y" /* yacc.c:1646  */
    { (yyval.real) = (yyvsp[-2].entero) + (yyvsp[0].real);}
#line 1590 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 37:
#line 131 "sintactico.y" /* yacc.c:1646  */
    { (yyval.real) = (yyvsp[-2].entero) - (yyvsp[0].real);}
#line 1596 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 38:
#line 132 "sintactico.y" /* yacc.c:1646  */
    {	(yyval.real) = (yyvsp[-2].entero) / (yyvsp[0].real);}
#line 1602 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 39:
#line 133 "sintactico.y" /* yacc.c:1646  */
    { (yyval.real) = (yyvsp[-2].real) / (yyvsp[0].entero);}
#line 1608 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 40:
#line 134 "sintactico.y" /* yacc.c:1646  */
    { (yyval.real) = (yyvsp[-2].entero) * (yyvsp[0].real);}
#line 1614 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 41:
#line 135 "sintactico.y" /* yacc.c:1646  */
    { (yyval.real) = (yyvsp[-2].real) * (yyvsp[0].entero);}
#line 1620 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 42:
#line 136 "sintactico.y" /* yacc.c:1646  */
    { (yyval.real) = pow((yyvsp[-2].entero),(yyvsp[0].real));}
#line 1626 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 43:
#line 137 "sintactico.y" /* yacc.c:1646  */
    { (yyval.real) = pow((yyvsp[-2].real),(yyvsp[0].entero));}
#line 1632 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 44:
#line 138 "sintactico.y" /* yacc.c:1646  */
    { (yyval.real) = fmod((yyvsp[-2].entero),(yyvsp[0].real));}
#line 1638 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 45:
#line 139 "sintactico.y" /* yacc.c:1646  */
    { (yyval.real) = fmod((yyvsp[-2].real),(yyvsp[0].entero));}
#line 1644 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 46:
#line 142 "sintactico.y" /* yacc.c:1646  */
    {(yyval.cadena) = (yyvsp[0].cadena); }
#line 1650 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 47:
#line 143 "sintactico.y" /* yacc.c:1646  */
    {(yyval.cadena) = concat((yyvsp[-2].cadena), (yyvsp[0].cadena));}
#line 1656 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 48:
#line 144 "sintactico.y" /* yacc.c:1646  */
    { (yyval.cadena) = obtenerValor((yyvsp[0].cadena));}
#line 1662 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 49:
#line 147 "sintactico.y" /* yacc.c:1646  */
    {(yyval.entero) = 0; }
#line 1668 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 50:
#line 148 "sintactico.y" /* yacc.c:1646  */
    {(yyval.entero) = 1;}
#line 1674 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 51:
#line 149 "sintactico.y" /* yacc.c:1646  */
    { (yyval.entero)= 2;}
#line 1680 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 52:
#line 153 "sintactico.y" /* yacc.c:1646  */
    { (yyval.entero) = compararIF((yyvsp[-3].entero),(yyvsp[-1].entero),(yyvsp[-2].entero));}
#line 1686 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 53:
#line 154 "sintactico.y" /* yacc.c:1646  */
    { (yyval.entero) = compararIF((yyvsp[-3].real),(yyvsp[-1].real),(yyvsp[-2].entero));}
#line 1692 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 54:
#line 155 "sintactico.y" /* yacc.c:1646  */
    { (yyval.entero) = compararIF((yyvsp[-3].real),(yyvsp[-1].entero),(yyvsp[-2].entero));}
#line 1698 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 55:
#line 156 "sintactico.y" /* yacc.c:1646  */
    { (yyval.entero) = compararIF((yyvsp[-3].entero),(yyvsp[-1].real),(yyvsp[-2].entero));}
#line 1704 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 56:
#line 160 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Operaciones incompatibles");}
#line 1710 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 57:
#line 161 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Operaciones incompatibles");}
#line 1716 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 58:
#line 162 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Operaciones incompatibles");}
#line 1722 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 59:
#line 163 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Operaciones incompatibles");}
#line 1728 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 60:
#line 164 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Operaciones incompatibles");}
#line 1734 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 61:
#line 166 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Operaciones incompatibles");}
#line 1740 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 62:
#line 167 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Operaciones incompatibles");}
#line 1746 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 63:
#line 168 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Operaciones incompatibles");}
#line 1752 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 64:
#line 169 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Operaciones incompatibles");}
#line 1758 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 65:
#line 171 "sintactico.y" /* yacc.c:1646  */
    {yyerror("La variable no ha sido declarda");}
#line 1764 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 66:
#line 172 "sintactico.y" /* yacc.c:1646  */
    {yyerror("La variable no ha sido declarda");}
#line 1770 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 67:
#line 173 "sintactico.y" /* yacc.c:1646  */
    {yyerror("La variable no ha sido declarda");}
#line 1776 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 68:
#line 175 "sintactico.y" /* yacc.c:1646  */
    {yyerror("La variable no ha sido declarda");}
#line 1782 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 69:
#line 176 "sintactico.y" /* yacc.c:1646  */
    {yyerror("La variable no ha sido declarda");}
#line 1788 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 70:
#line 177 "sintactico.y" /* yacc.c:1646  */
    {yyerror("La variable no ha sido declarda");}
#line 1794 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 71:
#line 179 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Operaciones incompatibles");}
#line 1800 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 72:
#line 180 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Operaciones incompatibles");}
#line 1806 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 73:
#line 181 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Operaciones incompatibles");}
#line 1812 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 74:
#line 182 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Operaciones incompatibles");}
#line 1818 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 75:
#line 184 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Operaciones incompatibles");}
#line 1824 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 76:
#line 185 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Operaciones incompatibles");}
#line 1830 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 77:
#line 186 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Operaciones incompatibles");}
#line 1836 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 78:
#line 187 "sintactico.y" /* yacc.c:1646  */
    {yyerror("Operaciones incompatibles");}
#line 1842 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 79:
#line 188 "sintactico.y" /* yacc.c:1646  */
    {yyerror("La variable no ha sido declarda");}
#line 1848 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 80:
#line 192 "sintactico.y" /* yacc.c:1646  */
    {(yyval.cadena) = (yyvsp[0].cadena);}
#line 1854 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 81:
#line 193 "sintactico.y" /* yacc.c:1646  */
    {(yyval.cadena) = (yyvsp[0].cadena);}
#line 1860 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 82:
#line 194 "sintactico.y" /* yacc.c:1646  */
    {(yyval.cadena) = (yyvsp[0].cadena);}
#line 1866 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 88:
#line 209 "sintactico.y" /* yacc.c:1646  */
    { agregarSimbolo((yyvsp[0].cadena),(yyvsp[-1].entero),NULL);}
#line 1872 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 89:
#line 210 "sintactico.y" /* yacc.c:1646  */
    { 
											cade = (char*) realloc(cade,sizeof((yyvsp[0].entero))/2);
											sprintf(cade,"%d",(yyvsp[0].entero)); 
											if(validarTipo((yyvsp[-3].entero),0) == 1){
												agregarSimbolo((yyvsp[-2].cadena),(yyvsp[-3].entero),cade);
											}
										}
#line 1884 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 90:
#line 217 "sintactico.y" /* yacc.c:1646  */
    { 
											cade = (char*) realloc(cade,sizeof((yyvsp[0].real))/2);
											sprintf(cade,"%f",(yyvsp[0].real)); 
											if(validarTipo((yyvsp[-3].entero),1) == 1){
												agregarSimbolo((yyvsp[-2].cadena),(yyvsp[-3].entero),cade);
											}
										}
#line 1896 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 91:
#line 224 "sintactico.y" /* yacc.c:1646  */
    {
											if(validarTipo((yyvsp[-3].entero),2) == 1){
												agregarSimbolo((yyvsp[-2].cadena),(yyvsp[-3].entero),(yyvsp[0].cadena));
											}
										}
#line 1906 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 92:
#line 229 "sintactico.y" /* yacc.c:1646  */
    {yyerror("La variable ya ha sido declarda");}
#line 1912 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 93:
#line 230 "sintactico.y" /* yacc.c:1646  */
    {yyerror("La variable ya ha sido declarda");}
#line 1918 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 94:
#line 231 "sintactico.y" /* yacc.c:1646  */
    {yyerror("La variable ya ha sido declarda");}
#line 1924 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 95:
#line 232 "sintactico.y" /* yacc.c:1646  */
    {yyerror("La variable ya ha sido declarda");}
#line 1930 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 96:
#line 238 "sintactico.y" /* yacc.c:1646  */
    {
									cade = (char*) realloc(cade,sizeof((yyvsp[0].entero))/2);
									sprintf(cade,"%d",(yyvsp[0].entero)); 
									if(validarTipoVar((yyvsp[-2].cadena),0) == 1){
										actualizar(&head,(yyvsp[-2].cadena),cade);
									}
									mostrarLista(head);
								}
#line 1943 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 97:
#line 246 "sintactico.y" /* yacc.c:1646  */
    {
									cade = (char*) realloc(cade,sizeof((yyvsp[0].real))/2);
									sprintf(cade,"%f",(yyvsp[0].real)); 
									if(validarTipoVar((yyvsp[-2].cadena),1) == 1){
										actualizar(&head,(yyvsp[-2].cadena),cade);
									}
									mostrarLista(head);
								}
#line 1956 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 98:
#line 254 "sintactico.y" /* yacc.c:1646  */
    {
									if(validarTipoVar((yyvsp[-2].cadena),2) == 1){
										actualizar(&head,(yyvsp[-2].cadena),(yyvsp[0].cadena));
									}
									mostrarLista(head);
								}
#line 1967 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 99:
#line 260 "sintactico.y" /* yacc.c:1646  */
    {}
#line 1973 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 100:
#line 261 "sintactico.y" /* yacc.c:1646  */
    {yyerror("La variable no ha sido declarda");}
#line 1979 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 101:
#line 262 "sintactico.y" /* yacc.c:1646  */
    {yyerror("La variable no ha sido declarda");}
#line 1985 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 102:
#line 263 "sintactico.y" /* yacc.c:1646  */
    {yyerror("La variable no ha sido declarda");}
#line 1991 "sintactico.tab.c" /* yacc.c:1646  */
    break;

  case 103:
#line 264 "sintactico.y" /* yacc.c:1646  */
    {yyerror("La variable no ha sido declarda");}
#line 1997 "sintactico.tab.c" /* yacc.c:1646  */
    break;


#line 2001 "sintactico.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 272 "sintactico.y" /* yacc.c:1906  */


int agregarSimbolo(char * nomb, int tipo, char * val){
	
		if (val == NULL)
			switch(tipo){
				case 0:
					insertar(&head,nomb,tipo,"0");
				break;
				case 1:
					insertar(&head,nomb,tipo,"0.0");
				break;
				case 2:
					insertar(&head,nomb,tipo," ");
			}
		else
			insertar(&head,nomb,tipo,val);

		mostrarLista(head);
		return 1;
}



int main() {
	head = (Lista*)malloc(sizeof(Lista));
	head -> var = NULL;
	yyparse();
}


char* obtenerValor(char *nomb){
	return buscar(head,nomb)->valor;
}

int validarTipo(int tipo,int tipoOP){
	if(tipo < 2 && tipoOP == 2){				
		yyerror("Tipos de dato incompatibles");	
		return 0;											
	}else if( tipo == 2 && tipo != tipoOP){//si es cadena y se le asigna otra cosa incomp
		yyerror("Tipos de dato incompatibles");	
		return 0;
	}
	return 1;
}


int compararIF(float op1,float op2, int oper){
	switch(oper){
		case 0:
			if (op1 < op2){
				return 1;
			}else{
				return 0;
			}
		break;
		case 1:
			if (op1 > op2){
				return 1;
			}else{
				return 0;
			}
		break;
		case 2:
			if (op1 == op2){
				return 1;
			}else{
				return 0;
			}
		break;
	}
	return 0;
}

int validarTipoVar(char *nomb,int tipoOP){
	aux = buscar(head,nomb);
	if(aux){
		if(validarTipo(aux->tipo, tipoOP)==1){
			return 1;
		}
	}else{
		yyerror("La variable no ha sido declarada");
		return 0;
	}
}

void imprimirBool(int c){
	if(c == 1){
		printf("true\n");
	}else{
		printf("false\n");
	}
}

             
yyerror (char *s)
{
  printf ("--%s--\n", s);
}
            
int yywrap()  
{  
  return 1;  
}  
