all:
	flex lexico.l
	bison -d sintactico.y 
	gcc Lista.c lex.yy.c sintactico.tab.c -lfl -lm -o a
clean:
	rm lex.yy.c
	rm sintactico.tab.c
	rm sintactico.tab.h
	rm a
	clear 