%{
#include "Lista.h"
#include <math.h>


Nodo *aux;
%}

%{
   	Lista *head;
   	char *cade;

	char * concat(char * a, char * b) {
	cade = (char*) malloc(sizeof(a)+sizeof(b));
	strcpy(cade, ""); 
	strcat(cade,  a); 
	strcat(cade,  b); 
	return strdup(cade);    
}

  	

%}

/*********************************************]]****DECLRACIONES_BISON*******************************************************************************/
%union{
	int entero;
	float real;
	char* cadena;
}

%token <entero> ENTERO
%token <cadena> CADENA
%token <cadena> INC_OP
%token <cadena> FPOW
%token <entero> IF
%token <real> REAL

%token <entero> TIPO_DATO

%token <cadena>  INTID
%token <cadena>  UNDECLRID
%token <cadena>  FLOATID
%token <cadena>  STRINGID
%token <entero> MENOR
%token <entero> MAYOR




%token IGUAL
%token MAS
%token MENOS
%token POR
%token ENTRE
%token POT
%token MOD
%token PA
%token PC
%token COMA

%token PTCOMA


%type <cadena> var_declr	
%type <entero> exp_ent	
%type <real> exp_real 
%type <cadena> exp_cad  
%type <cadena> inc_op
%type <entero> func_if
%type <entero> comp


		
%left MAS MENOS 
%left DIV MOD POR
%left POW FPOW
%left IGUAL
             
/*************************************************************GRAMATICAS*******************************************************************************/
%%
             
input:    /* cadena vacía */
        | input line             
;

line:     '\n'
        | operacion PTCOMA '\n' 
        | exp_cad	PTCOMA'\n'
        | declr  PTCOMA '\n'
        | asign  PTCOMA '\n'       
		| INTID PTCOMA '\n'			{ printf("Valor: %d\n",atoi(obtenerValor($1)));}
		| FLOATID PTCOMA '\n'		{ printf("Valor: %f\n",atof(obtenerValor($1)));}
		| STRINGID PTCOMA '\n'		{ printf("Valor: %s\n",obtenerValor($1));}
		| inc_op PTCOMA'\n' 
		| func_if PTCOMA '\n' 		{imprimirBool($1);}
;

operacion: exp_ent {printf("Resultado: %d\n",$1);}
	| exp_real {printf("Resultado: %f\n",$1);}
;


             

exp_ent:     ENTERO				 { $$ = $1; }
	| MENOS ENTERO				 { $$ = ($2)*(-1);}
	| exp_ent MAS exp_ent	     { $$ = $1 + $3;}
	| exp_ent MENOS exp_ent	     { $$ = $1 - $3;}
	| exp_ent POR exp_ent     	 { $$ = $1 * $3;}
	| exp_ent ENTRE exp_ent      { $$ = $1 / $3;}
	| exp_ent POT exp_ent    	 { $$ = pow($1,$3);}
	| exp_ent MOD exp_ent    	 { $$ = fmod($1,$3);}
	| FPOW PA exp_ent COMA exp_ent PC{ $$ = pow($3,$5);}
	| INTID 					 { $$ = atoi(obtenerValor($1));}
;


exp_real: REAL {$$ = $1;}
	| MENOS REAL				{ $$ = ($2)*(-1);}
	| exp_real MAS exp_real 	{ $$ = $1 + $3;}
	| exp_real MENOS exp_real 	{ $$ = $1 - $3;}
	| exp_real ENTRE exp_real 	{ $$ = $1 / $3;	}
	| exp_real POR exp_real 	{ $$ = $1 * $3;}
	| exp_real POT exp_real 	{ $$ = pow($1,$3);}
	| exp_real MOD exp_real 	{ $$ = fmod($1,$3);}
	| FLOATID 					{ $$ = atof(obtenerValor($1));}

	| exp_real MAS exp_ent      { $$ = $1 + $3;}
	| exp_ent MAS exp_real     	{ $$ = $1 + $3;}
	| exp_ent MENOS exp_real    { $$ = $1 - $3;}
	| exp_ent ENTRE exp_real    {	$$ = $1 / $3;}
	| exp_real ENTRE exp_ent    { $$ = $1 / $3;}
	| exp_ent POR exp_real     	{ $$ = $1 * $3;}
	| exp_real POR exp_ent      { $$ = $1 * $3;}
	| exp_ent POT exp_real		{ $$ = pow($1,$3);}
	| exp_real POT exp_ent 		{ $$ = pow($1,$3);}
	| exp_ent MOD exp_real 		{ $$ = fmod($1,$3);}
	| exp_real MOD exp_ent   	{ $$ = fmod($1,$3);}
;

exp_cad: CADENA 			{$$ = $1; }
	| exp_cad MAS exp_cad 	{$$ = concat($1, $3);}
	| STRINGID				{ $$ = obtenerValor($1);}	
;

comp: MENOR {$$ = 0; }
	| MAYOR {$$ = 1;}
	| IGUAL IGUAL { $$= 2;}
;


func_if: IF PA exp_ent comp exp_ent PC { $$ = compararIF($3,$5,$4);}
	| IF PA exp_real comp exp_real PC { $$ = compararIF($3,$5,$4);}
	| IF PA exp_real comp exp_ent PC { $$ = compararIF($3,$5,$4);}
	| IF PA exp_ent comp exp_real PC { $$ = compararIF($3,$5,$4);}

;

inc_op:  inc_op opers exp_ent {yyerror("Operaciones incompatibles");}
	| exp_ent opers inc_op {yyerror("Operaciones incompatibles");}
	| inc_op opers exp_real {yyerror("Operaciones incompatibles");}
	| exp_real opers inc_op {yyerror("Operaciones incompatibles");}	
	| inc_op opers exp_cad {yyerror("Operaciones incompatibles");}

	| inc_op MAS exp_ent {yyerror("Operaciones incompatibles");}
	| exp_ent MAS inc_op {yyerror("Operaciones incompatibles");}
	| inc_op MAS exp_real {yyerror("Operaciones incompatibles");}
	| exp_real MAS inc_op {yyerror("Operaciones incompatibles");}

	| exp_real opers UNDECLRID{yyerror("La variable no ha sido declarda");}
	| exp_ent opers UNDECLRID{yyerror("La variable no ha sido declarda");}
	| exp_cad opers UNDECLRID{yyerror("La variable no ha sido declarda");}

	| exp_real MAS UNDECLRID{yyerror("La variable no ha sido declarda");}
	| exp_ent MAS UNDECLRID{yyerror("La variable no ha sido declarda");}
	| exp_cad MAS UNDECLRID{yyerror("La variable no ha sido declarda");}

	| exp_cad opers exp_ent {yyerror("Operaciones incompatibles");}
	| exp_ent opers exp_cad {yyerror("Operaciones incompatibles");}
	| exp_cad opers exp_real {yyerror("Operaciones incompatibles");}
	| exp_real opers exp_cad {yyerror("Operaciones incompatibles");}

	| exp_cad MAS exp_ent {yyerror("Operaciones incompatibles");}
	| exp_ent MAS exp_cad {yyerror("Operaciones incompatibles");}
	| exp_real MAS exp_cad {yyerror("Operaciones incompatibles");}
	| exp_cad MAS exp_real {yyerror("Operaciones incompatibles");}
	| UNDECLRID {yyerror("La variable no ha sido declarda");}
;


var_declr: INTID {$$ = $1;}
	| FLOATID {$$ = $1;}
	| STRINGID {$$ = $1;}
;

opers: MENOS	
	| POR 
	| ENTRE 
	| POT 
	| MOD 
;





/***************************Declarcion de variables*************************************************/
declr:	TIPO_DATO UNDECLRID  { agregarSimbolo($2,$1,NULL);}
	| TIPO_DATO UNDECLRID IGUAL exp_ent { 
											cade = (char*) realloc(cade,sizeof($4)/2);
											sprintf(cade,"%d",$4); 
											if(validarTipo($1,0) == 1){
												agregarSimbolo($2,$1,cade);
											}
										}
	| TIPO_DATO UNDECLRID IGUAL exp_real  { 
											cade = (char*) realloc(cade,sizeof($4)/2);
											sprintf(cade,"%f",$4); 
											if(validarTipo($1,1) == 1){
												agregarSimbolo($2,$1,cade);
											}
										}		
	| TIPO_DATO UNDECLRID IGUAL exp_cad  {
											if(validarTipo($1,2) == 1){
												agregarSimbolo($2,$1,$4);
											}
										}
	| TIPO_DATO var_declr IGUAL inc_op		{yyerror("La variable ya ha sido declarda");}
	| TIPO_DATO var_declr IGUAL exp_ent		{yyerror("La variable ya ha sido declarda");}
	| TIPO_DATO var_declr IGUAL exp_real	{yyerror("La variable ya ha sido declarda");}
	| TIPO_DATO var_declr IGUAL exp_cad		{yyerror("La variable ya ha sido declarda");}
;

/***************************Asignacion*************************************************/


asign: var_declr IGUAL exp_ent  {
									cade = (char*) realloc(cade,sizeof($3)/2);
									sprintf(cade,"%d",$3); 
									if(validarTipoVar($1,0) == 1){
										actualizar(&head,$1,cade);
									}
									mostrarLista(head);
								}
	| var_declr IGUAL exp_real  {
									cade = (char*) realloc(cade,sizeof($3)/2);
									sprintf(cade,"%f",$3); 
									if(validarTipoVar($1,1) == 1){
										actualizar(&head,$1,cade);
									}
									mostrarLista(head);
								}
	| var_declr IGUAL exp_cad  {
									if(validarTipoVar($1,2) == 1){
										actualizar(&head,$1,$3);
									}
									mostrarLista(head);
								}
	| var_declr IGUAL inc_op  	{}
	| UNDECLRID IGUAL exp_cad 	{yyerror("La variable no ha sido declarda");}
	| UNDECLRID IGUAL exp_ent 	{yyerror("La variable no ha sido declarda");}
	| UNDECLRID IGUAL exp_real 	{yyerror("La variable no ha sido declarda");}
	| UNDECLRID IGUAL inc_op 	{yyerror("La variable no ha sido declarda");}


;



/*********************************************************FIN_GRAMATICAS*******************************************************************************/
%%

int agregarSimbolo(char * nomb, int tipo, char * val){
	
		if (val == NULL)
			switch(tipo){
				case 0:
					insertar(&head,nomb,tipo,"0");
				break;
				case 1:
					insertar(&head,nomb,tipo,"0.0");
				break;
				case 2:
					insertar(&head,nomb,tipo," ");
			}
		else
			insertar(&head,nomb,tipo,val);

		mostrarLista(head);
		return 1;
}



int main() {
	head = (Lista*)malloc(sizeof(Lista));
	head -> var = NULL;
	yyparse();
}


char* obtenerValor(char *nomb){
	return buscar(head,nomb)->valor;
}

int validarTipo(int tipo,int tipoOP){
	if(tipo < 2 && tipoOP == 2){				
		yyerror("Tipos de dato incompatibles");	
		return 0;											
	}else if( tipo == 2 && tipo != tipoOP){//si es cadena y se le asigna otra cosa incomp
		yyerror("Tipos de dato incompatibles");	
		return 0;
	}
	return 1;
}


int compararIF(float op1,float op2, int oper){
	switch(oper){
		case 0:
			if (op1 < op2){
				return 1;
			}else{
				return 0;
			}
		break;
		case 1:
			if (op1 > op2){
				return 1;
			}else{
				return 0;
			}
		break;
		case 2:
			if (op1 == op2){
				return 1;
			}else{
				return 0;
			}
		break;
	}
	return 0;
}

int validarTipoVar(char *nomb,int tipoOP){
	aux = buscar(head,nomb);
	if(aux){
		if(validarTipo(aux->tipo, tipoOP)==1){
			return 1;
		}
	}else{
		yyerror("La variable no ha sido declarada");
		return 0;
	}
}

void imprimirBool(int c){
	if(c == 1){
		printf("true\n");
	}else{
		printf("false\n");
	}
}

             
yyerror (char *s)
{
  printf ("--%s--\n", s);
}
            
int yywrap()  
{  
  return 1;  
}  