%{
        #include "sintactico.tab.h"
        #include "Lista.h"
        extern Lista *head;
%}
Cadena \"[^"\n]*\"
LETRA [a-zA-Z]
DIGITO [0-9]
INT {DIGITO}+
FLOAT {INT}"."{DIGITO}*
TIPO_DATO "float"|"int"|"String"
ID_VAR ("_"|{LETRA})({LETRA}|{DIGITO}|"_")*
MAY_MEN_IG "<"|">"|"=="



%%
{INT}       {      
                yylval.entero = atoi(yytext);
                return (ENTERO);
            }

{FLOAT}     {
                yylval.real=atof(yytext);
                return (REAL);
            }
{Cadena}    {
                int i;
                for(i=1;i<strlen(yytext)-1;i++)
                    yytext[i-1] = yytext[i];                        
                yytext[i-1] = '\0';                
                yylval.cadena=strdup(yytext);
                return (CADENA);
            }
"-"         {
                return (MENOS);
            }
"="         {
                return (IGUAL);
            }    
"+"         {
                return (MAS);
            }

"/"         {
                return (ENTRE);
            }
"*"         {
                return (POR);
            }

"^"         {
                return (POT);
            } 
"%"         {
                 return (MOD);
            }
"pow"       {
                yylval.entero = 0;
                return (FPOW);
            }
"<"         {
                return(MENOR);
            }
">"         {
                return(MAYOR);
            }
"if"        {
                return (IF);
            } 
{TIPO_DATO} {
                if (strcmp(yytext, "int") == 0) {
                    yylval.entero = 0;
                }else if (strcmp(yytext,  "float") == 0) {
                    yylval.entero = 1;      
                }else if (strcmp(yytext,  "String") == 0) {
                    yylval.entero = 2;      
                }
                return (TIPO_DATO);
            }
"("         {
                return (PA);
            } 
")"         {
                return (PC);
            } 
","         {
                return (COMA);
            } 
";"         {
                return (PTCOMA);
            }
{ID_VAR}	{
        		Nodo * ans = buscar(head, yytext);
        	        yylval.cadena = strdup(yytext);
        	        if (!ans) {
        	               return (UNDECLRID);
                        }else if (ans->tipo == 0) {
        	               	return (INTID);
                        }else if ( ans->tipo == 1) {
        	               return (FLOATID);	
                        }else if ( ans->tipo == 2) {
                                return (STRINGID);	
                }
            }
"\n"        {
                return (yytext[0]);
            }



.               ;
%%
